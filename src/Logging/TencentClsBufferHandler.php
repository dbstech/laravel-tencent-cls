<?php

namespace Magiclamp\TencentCls\Logging;

use Monolog\Handler\BufferHandler;
use Monolog\Logger;

class TencentClsBufferHandler extends BufferHandler
{
    /**
     * Take same argument as BufferHandler, and also resolve an instance of TencentClsHandler to pass in
     *
     * @param integer $bufferLimit
     * @param [type] $level
     * @param boolean $bubble
     * @param boolean $flushOnOverflow
     */
    public function __construct(int $bufferLimit = 0, $level = Logger::DEBUG, bool $bubble = true, bool $flushOnOverflow = false)
    {
        $handler = resolve(TencentClsHandler::class);
        parent::__construct($handler, $bufferLimit, $level, $bubble, $flushOnOverflow);
    }
}
