<?php

namespace Magiclamp\TencentCls\Logging;

use Magiclamp\TencentCls\Exceptions\TencentClsException;
use Magiclamp\TencentCls\Jobs\PushLogs;
use Magiclamp\TencentCls\TencentClsService;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class TencentClsHandler extends AbstractProcessingHandler
{
    protected $clsService;
    protected $topicId;
    protected $host;
    protected $queue;

    public function __construct(TencentClsService $clsService, array $options = [], $level = Logger::DEBUG, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
        $this->clsService = $clsService;
        $this->topicId = $options['topic_id'];
        $this->host = $options['host'];
        $this->queue = $options['queue'] ?? 'default';
    }

    protected function write(array $record): void
    {
        $this->pushLogs([$record]);
    }

    /**
     * Push a batch of log records
     *
     * @param array $records
     * @return void
     */
    public function handleBatch(array $records): void
    {
        $this->pushLogs($records);
    }

    protected function pushLogs(array $records)
    {
        // Must sanitize records before dispatching to queue
        // Otherwise dispatch may fail if some data is unserializable
        $records = $this->sanitizeRecords($records);
        $source = gethostname();
        if ($this->queue) {
            PushLogs::dispatch($this->host, $this->topicId, $source, $records)->onQueue($this->queue);
        } else {
            // Rethrow all handler exceptions as TencentClsException
            // so that the laravel app can put it in dontReport and avoid recursive log -> error cycle
            try {
                $this->clsService->pushLogs($this->host, $this->topicId, $source, $records);
            } catch (\Exception $e) {
                throw new TencentClsException($e->getMessage());
            }
        }
    }

    /**
     * Cast record context values to string
     * This must be done because context is user supplied
     * and if it can't be serialized for queue, then there will be fatal error
     * Laravel also pass exception into context, which usually can't be serialized properly for the queue (e.g. Closures)
     * So we just cast it to a string here to avoid errors on exception
     * @param array $record
     * @return array
     */
    protected function sanitizeContext(array $context = []): array
    {
        return array_map(function ($val) {
            return TencentClsService::castToString($val);
        }, $context);
    }

    /**
     * Sanitize all log record to ensure they can be serialized for queue
     *
     * @param array $records
     * @return array
     */
    protected function sanitizeRecords(array $records): array
    {
        return array_map(function ($record) {
            // Context is user supplied so it needs to be sanitized
            if (isset($record['context'])) {
                $record['context'] = $this->sanitizeContext($record['context']);
            }
            return $record;
        }, $records);
    }
}
