<?php

namespace Magiclamp\TencentCls;

use Illuminate\Support\ServiceProvider;
use Magiclamp\TencentCls\Logging\TencentClsHandler;

class TencentClsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // publish config
        $this->publishes([
            __DIR__.'/../config/tencentcls.php' => config_path('tencentcls.php'),
        ]);
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/logging.php', 'logging');
        $this->mergeConfigFrom(__DIR__.'/../config/tencentcls.php', 'tencentcls');

        $tencentCfg = $this->app['config']['tencentcls'];

        $this->app->singleton(TencentClsService::class, function ($app) use ($tencentCfg) {
            return new TencentClsService($tencentCfg['secret_id'], $tencentCfg['secret_key']);
        });

        $this->app->bind(TencentClsHandler::class, function ($app) use ($tencentCfg) {
            return new TencentClsHandler($app->make(TencentClsService::class), $tencentCfg);
        });
    }
}
