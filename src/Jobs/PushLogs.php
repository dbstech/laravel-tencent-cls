<?php

namespace Magiclamp\TencentCls\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Magiclamp\TencentCls\Exceptions\TencentClsException;
use Magiclamp\TencentCls\TencentClsService;

class PushLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $records;
    protected $host;
    protected $topicId;
    protected $source;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $host, string $topicId, string $source, array $records)
    {
        $this->host = $host;
        $this->topicId = $topicId;
        $this->source = $source;
        $this->records = $records;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TencentClsService $clsService)
    {
        try {
            $clsService->pushLogs($this->host, $this->topicId, $this->source, $this->records);
        } catch (\Exception $e) {
            throw new TencentClsException($e->getMessage());
        }
    }
}
