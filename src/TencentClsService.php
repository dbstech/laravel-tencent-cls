<?php

namespace Magiclamp\TencentCls;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Magiclamp\TencentCls\Exceptions\TencentClsException;
use Magiclamp\TencentCls\Protobuf\Log;
use Magiclamp\TencentCls\Protobuf\Log\Content;
use Magiclamp\TencentCls\Protobuf\LogGroup;
use Magiclamp\TencentCls\Protobuf\LogGroupList;

class TencentClsService
{
    const MAX_LOGS_IN_GROUP = 10000;

    // Tencent API key found in https://console.cloud.tencent.com/cam/capi
    protected $secretId;
    protected $secretKey;

    protected $httpClient;

    public function __construct(string $secretId, string $secretKey)
    {
        $this->secretId = $secretId;
        $this->secretKey = $secretKey;

        $this->httpClient = new Client([
            'timeout' => 10,
        ]);
    }

    public function setSecretId(string $id)
    {
        $this->secretId = $id;
    }

    public function setSecretKey(string $key)
    {
        $this->secretKey = $key;
    }

    /**
     * Build the string required for Authorization header value
     * when making requests to authenticated endpoints
     *
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param array $queryParams
     * @return string
     */
    public function buildAuthHeader(
        string $method,
        string $uri,
        array $headers,
        array $queryParams,
        string $qSignTime = null
    ): string {
        $authParams = $this->buildAuthParams($method, $uri, $headers, $queryParams, $qSignTime);

        // Encode like key=value, delimited by &
        $kvs = [];
        foreach ($authParams as $key => $value) {
            $kvs[] = "{$key}={$value}";
        }

        return implode('&', $kvs);
    }

    /**
     * Get the params needed to build the Authorization header for the given request parameters.
     *
     * URI includes starting slash
     *
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param array $queryParams
     * @return array
     */
    public function buildAuthParams(
        string $method,
        string $uri,
        array $headers,
        array $queryParams,
        string $qSignTime = null
    ): array {
        if (!$qSignTime) {
            // Sign time start
            $now = time();
            // Sign time expires in 10 minutes (arbitrary)
            $expire = $now + 600;
            $qSignTime = "{$now};{$expire}";
        }
        $qKeyTime = $qSignTime;

        // Header list needs to be sorted alphabetically and lower cased, and separated by ;
        $sanitizedHeaders = [];
        foreach ($headers as $key => $value) {
            $sanitizedHeaders[strtolower($key)] = $value;
        }
        // Both the key-value pairs and the keys need to be sorted
        ksort($sanitizedHeaders);
        $headerKeys = array_keys($sanitizedHeaders);
        sort($headerKeys);

        // URL query param list needs to be sorted alphabetically and lower cased, and separated by ;
        $sanitizedParams = [];
        foreach ($queryParams as $key => $value) {
            $sanitizedParams[strtolower($key)] = $value;
        }
        // Both the key-value pairs and the keys need to be sorted
        ksort($sanitizedParams);
        $queryParamKeys = array_keys($sanitizedParams);
        sort($queryParamKeys);

        $qSignAlgo = 'sha1';

        $signature = $this->sign($method, $uri, $sanitizedHeaders, $sanitizedParams, $qSignAlgo, $qSignTime, $qKeyTime);

        return [
            'q-sign-algorithm' => $qSignAlgo,
            'q-ak' => $this->secretId,
            'q-sign-time' => $qSignTime,
            'q-key-time' => $qKeyTime,
            'q-header-list' => implode(';', $headerKeys),
            'q-url-param-list' => implode(';', $queryParamKeys),
            'q-signature' => $signature,
        ];
    }

    /**
     * Return a signature for the given parameters
     *
     * @param string $method
     * @param string $uri
     * @param array $headers
     * @param array $queryParams
     * @param string $qSignAlgo
     * @param string $qSignTime
     * @param string $qKeyTime
     * @return string
     */
    public function sign(
        string $method,
        string $uri,
        array $headers,
        array $queryParams,
        string $qSignAlgo,
        string $qSignTime,
        string $qKeyTime
    ): string {
        $requestInfo = strtolower($method) . "\n"
            . $uri . "\n"
            . http_build_query($queryParams, '', '&', PHP_QUERY_RFC3986) . "\n"
            . http_build_query($headers, '', '&', PHP_QUERY_RFC3986) . "\n";

        $stringToSign = $qSignAlgo . "\n"
            . $qSignTime . "\n"
            . sha1($requestInfo) . "\n";

        $signKey = hash_hmac($qSignAlgo, $qKeyTime, $this->secretKey);

        $signature = hash_hmac($qSignAlgo, $stringToSign, $signKey);

        return $signature;
    }

    /**
     * Build log protobuffer object for the given log message
     *
     * @param string $level
     * @param string $message
     * @param integer $time
     * @param array $context
     * @return Log
     */
    public function buildLog(string $level, string $message, int $time, array $context = []): Log
    {
        // Insert log level and the main message
        // Context keys 'level' and 'message' will be overriden
        $context['level'] = $level;
        $context['message'] = $message;

        // Insert all content key-values
        foreach ($context as $key => $value) {
            // Ensure the context value is casted to string
            $strValue = self::castToString($value);
            // Only add to contents if both key and value are not empty (otherwise Tencent will return error)
            if ($key && $strValue) {
                $contents[] = new Content([
                    'key' => $key,
                    'value' => self::castToString($value),
                ]);
            }
        }

        return new Log([
            'time' => $time,
            'contents' => $contents,
        ]);
    }

    /**
     * Cast the given value to a string
     *
     * @param [type] $value
     * @return string
     */
    public static function castToString($value): string
    {
        try {
            return (string)$value;
        } catch (\Throwable $e) {
            // Try to JSON encode (works for arrays and objects)
            // We will return empty string on failure
            return (string)json_encode($value);
        }
    }

    /**
     * Build a log group from the given logs
     *
     * @param array $logs
     * @return LogGroup
     */
    public function buildLogGroup(string $source, array $logs): LogGroup
    {
        // Hardcode filename for now
        return new LogGroup([
            'logs' => $logs,
            'filename' => 'laravel.log',
            'source' => $source,
            // unused but proto3 doesn't support optional fields
            'contextFlow' => '',
        ]);
    }

    /**
     * Build log group list from the given logs
     *
     * @param array $logGroups
     * @return void
     */
    public function buildLogGroupList(string $source, array $logs): LogGroupList
    {
        // Max 10000 logs in a log group
        // So split into multiple log groups if there are too many logs
        $chunks = array_chunk($logs, self::MAX_LOGS_IN_GROUP);
        $logGroups = array_map(function ($logs) use ($source) {
            return $this->buildLogGroup($source, $logs);
        }, $chunks);
        return new LogGroupList([
            'logGroupList' => $logGroups,
        ]);
    }

    /**
     * Push the given monolog records to the given tencent cloud CLS topic ID
     *
     * @param string $topicId
     * @param array $records
     * @return array
     */
    public function pushLogs(string $host, string $topicId, string $source, array $records): bool
    {
        $headers = [
            'Host' => $host,
            'Content-Type' => 'application/x-protobuf',
        ];

        $queryParams = [
            'topic_id' => $topicId,
        ];

        $method = 'POST';
        $endpoint = '/structuredlog';

        // Build the Authorization header
        $auth = $this->buildAuthHeader($method, $endpoint, $headers, $queryParams);
        // Add to headers
        $headers['Authorization'] = $auth;

        // encode records to protobuf
        $logs = array_map(function ($record) {
            // Convert DateTimeImmutable to unix timestamp
            $timestamp = $record['datetime']->getTimestamp();
            // Merge user-supplied context and extra params from processor
            $context = array_merge($record['context'], $record['extra']);
            return $this->buildLog($record['level_name'], $record['message'], $timestamp, $context);
        }, $records);
        $logGroupList = $this->buildLogGroupList($source, $logs);

        // 200 OK means success
        try {
            $this->httpClient->request('POST', '/structuredlog', [
                'base_uri' => "https://{$host}",
                'query' => $queryParams,
                'body' => $logGroupList->serializeToString(),
                'headers' => $headers,
            ]);
        } catch (BadResponseException $e) {
            // Extract error code and message if server returned non 200 HTTP code
            if ($e->hasResponse()) {
                $body = (string)$e->getResponse()->getBody();
                $payload = json_decode($body, true);
                $code = $payload['errorcode'] ?? '';
                $msg = $payload['errormessage'] ?? '';
                // TODO: will this attempt to log..? might result in recursively logging error and failing
                throw new TencentClsException("[{$code}] {$msg}");
            } else {
                throw $e;
            }
        }

        return true;
    }

    /**
     * Push a singular monolog record to tencent cloud CLS
     *
     * @param string $topicId
     * @param array $record
     * @return void
     */
    public function pushLog(string $host, string $topicId, string $source, array $record)
    {
        return $this->pushLogs($host, $topicId, $source, [$record]);
    }

    /**
     * Search logs from tencent cloud CLS
     *
     * https://cloud.tencent.com/document/product/614/16875
     *
     * @param string $host
     * @param string $logsetId
     * @param string $topicIds Separate by comma (,)
     * @param string $startTime China timezone, format 2017-07-14 20:43:00
     * @param string $endTime
     * @param string $queryString
     * @param integer $limit max 100
     * @param string $context pagination
     * @param string $sort asc, desc (default)
     * @return \stdClass
     */
    public function searchLogs(
        string $host,
        string $logsetId,
        string $topicIds,
        string $startTime,
        string $endTime,
        string $queryString = '',
        int $limit = 10,
        string $context = '',
        string $sort = 'desc'
    ): \stdClass {
        $headers = [
            'Host' => $host,
        ];

        $queryParams = [
            'logset_id' => $logsetId,
            'topic_ids' => $topicIds,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'query_string' => $queryString,
            'limit' => $limit,
            'context' => $context,
            'sort' => $sort,
        ];

        $method = 'GET';
        $endpoint = '/searchlog';

        // Build the Authorization header
        $auth = $this->buildAuthHeader($method, $endpoint, $headers, $queryParams);
        // Add to headers
        $headers['Authorization'] = $auth;

        $response = $this->httpClient->request($method, $endpoint, [
            'base_uri' => "https://{$host}",
            'query' => $queryParams,
            'headers' => $headers,
        ]);

        return json_decode($response->getBody());
    }
}
