<?php

use Magiclamp\TencentCls\Protobuf\Log;
use Magiclamp\TencentCls\Protobuf\Log\Content;

require __DIR__ . '/vendor/autoload.php';

$logContent = new Content;
$logContent->setKey('message');
$logContent->setValue('test');
$log = new Log;
$log->setTime(time());
$log->setContents([$logContent]);
$string = $log->serializeToString();
var_dump($string);
