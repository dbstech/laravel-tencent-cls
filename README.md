# README #

This is a Laravel package for integrating Tencent CLS for laravel applications

### How to add this package to my laravel application? ###

1. Require dependency in laravel project `composer require magiclamp/laravel-tencent-cls`
2. To publish config files, run `php artisan vendor:publish` and select `TencentClsServiceProvider`. This will publish `config/tencentcls.php` and update `config/logging.php`
5. In `app/Exceptions/Handler.php`, add `Magiclamp\TencentCls\Exceptions\TencentClsException` to the list of `dontReport` exceptions. This is **important** because this stops the `TencentClsHandler` from logging exceptions to itself, which may result in a recursive log -> error -> log cycle.
3. In `config/tencentcls.php` configure Tencent Cloud API credentials and default topic and host to push logs to
4. In `config/logging.php`, add the `tencentcls` logging channel (see the example in the package `config/logging`) can be used like any other logging channel

### How to make changes to the protobuf schema?

Tencent CLS uses the Google protobuf binary protocol to transport logs via API. If changes need to be made to the schema, take the following steps:
1. First ensure you have the `protoc` compiler installed. This can be found at https://github.com/protocolbuffers/protobuf/releases (look for protoc-xxx archives)
2. Modify `LogGroupList.proto` as needed
3. Run `protoc --php_out=build LogGroupList.proto` to generate the PHP code for the protobuf schema. This will output files to `build` folder
4. We've predefined the autoload paths in `composer.json`, so we must copy the generated files into the correct folders:
** Copy `build/Magiclamp/TencentCls/Protobuf` folder into `src`.
** Copy `build/GPBMetadata` folder into `src/Protobuf`
** delete the `build` folder to cleanup

### Who do I talk to? ###

* kenley.c@mofadeng.com