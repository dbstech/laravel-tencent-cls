<?php

use Magiclamp\TencentCls\Logging\TencentClsBufferHandler;

return [
    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'tencentcls' => [
            'driver' => 'monolog',
            'handler' => TencentClsBufferHandler::class,
            'with' => [
                // Configure the buffer handler here
                'buffer_limit' => 20000,
                'flush_on_overflow' => true,
            ],
        ],
    ],
];
