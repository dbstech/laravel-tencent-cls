<?php

return [
    // Fill in the tencent cloud API credentials
    'secret_id' => '',
    'secret_key' => '',

    // only support push to one topic for now
    'topic_id' => '',
    'host' => '',

    // configure the queue name to push the logs to
    // falsey value means queue won't be used (logs will be pushed synchronously)
    'queue' => 'default',
];
